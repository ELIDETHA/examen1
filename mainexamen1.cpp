
// Online C++ Compiler - Build, Compile and Run your C++ programs online in your favorite browser

#include<iostream>
#include<cmath>
using namespace std;
// Pregunta 1: Complete la declaración de la clase base Figura
class Figura {
public:
virtual double calcularArea() = 0;
};
// Pregunta 2: Complete la declaración de la clase Triangulo que hereda de Figura
class Triangulo : public Figura {
private:
// Atributos
double lado1;
double lado2;
double lado3;
public:
// Constructor
// Método para calcular el área
Triangulo(double lado1, double lado2, double lado3) : lado1(lado1), lado2(lado2), lado3(lado3) {}
double calcularArea() override {
    //usamos la formula de Heron para calcular el área de un Triangulo
    double s= (lado1 + lado2 + lado3) / 2;
    double area = sqrt(s * (s-lado1)* (s - lado2) * (s -lado3));
    return area;
}
};
// Pregunta 3: Complete la declaración de la clase Cuadrado que hereda de Figura
class Cuadrado : public Figura {
private:
// Atributo
double lado;
public:
// Constructor
// Método para calcular el área
Cuadrado(double lado) : lado(lado) {}
double calcularArea() override {
    return lado * lado; //area del cuadrado = lado * lado
}
};
// Pregunta 4: Complete el código en la función main
int main() {
// Crear instancias de Triangulo y Cuadrado
// Mostrar el cálculo del área de cada uno
Triangulo triangulo(8.0, 4.0, 7.0);  // Ejemplo de un triángulo con lados de longitud 8, 4 y 7.
    Cuadrado cuadrado(9.0);             // Ejemplo de un cuadrado con un lado de longitud 9.

    double areaTriangulo = triangulo.calcularArea();
    double areaCuadrado = cuadrado.calcularArea();

    std::cout << "Area del triangulo: " << areaTriangulo << std::endl;
    std::cout << "Area del cuadrado: " << areaCuadrado << std::endl;


    return 0;
}

//Explique brevemente en que consiste la herencia en C++ y cuales son sus beneficios en la programacion orientada a objetos.
//Herencia en C++ es un mecanismo que permite crear nuevas clases a partir de clases existentes, de manera que las nuevas clases heredan los atributos y métodos de las clases existentes.
//la herencia en C++ es un mecanismo poderoso que permite crear nuevas clases a partir de clases existentes, lo que ahorra tiempo y esfuerzo en la programación y facilita la creación de jerarquías de clases que representan conceptos abstractos y complejos de manera más clara y organizada.
//Beneficios:
//Polimorfismo: La herencia permite crear objetos de diferentes clases que pueden ser tratados de manera uniforme, lo que facilita la programación y el mantenimiento del código.
//Abstracción: La herencia permite crear jerarquías de clases que representan conceptos abstractos y complejos de manera más clara y organizada.
